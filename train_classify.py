import cv2
import glob
import random
import numpy as np

emotions = ["sadness","happy"] 
fishface = cv2.createFisherFaceRecognizer() 

data = {}

def get_files(emotion): 
    files = glob.glob("dataset\\%s\\*" %emotion)
    random.shuffle(files)
    training = files[:int(len(files)*0.8)] 
    prediction = files[-int(len(files)*0.2):] 
    return training, prediction   

def make_sets():
    training_data = []
    training_labels = []
    prediction_data = []
    prediction_labels = []
    for emotion in emotions:
        training, prediction = get_files(emotion)
       
        for item in training:
            image = cv2.imread(item) 
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
            training_data.append(gray) 
            training_labels.append(emotions.index(emotion))
            prediction_labels.append(emotions.index(emotion))

    return training_data, training_labels, prediction_data, prediction_labels


training_data, training_labels, prediction_data, prediction_labels = make_sets()
fishface.train(training_data, np.asarray(training_labels))
image = cv2.imread('Raam_sorted_sad.jpg')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

pred, conf = fishface.predict(gray)
print pred

if pred == 0:
    print 'sad'
else:
    print 'happy'



 

